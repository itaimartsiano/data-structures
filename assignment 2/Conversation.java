public class Conversation {
	public String ConversationGroupName;
	private Message Head;
	public Conversation Next;

	private Message Tail;

	public Conversation(String conversionGroupName) {
		this.ConversationGroupName = conversionGroupName;
	}

	public void insertMessage(Message msg) {
		if (Head == null) {
			Head = msg;
			Tail = msg;
		} else {
			Tail.Next = msg;
			Tail = Tail.Next;
		}
	}

	public Message[] searchMessage(String text) {
	
		// look for the size of the messages that fit to the array
		Message curr = Head;
		int counter = 0;

		while (curr != null) {
			if (curr.Text.contains(text)) {
				counter++;
			}
			curr = curr.Next;
		}
		
		if(counter==0){
			return null;
		}
	
		// new Message array [size]
		Message[] answer = new Message[counter];
		curr = Head;
	
		int i=0;
		while (curr != null) {
			if (curr.Text.contains(text)) {
				answer[i] = curr;
				i++;
			}
			curr = curr.Next;
		}

		return answer;
	}

	public String toString() {
		String retVal = ConversationGroupName + "\n";
		Message temp = Head;
		while (temp != null) {
			retVal += temp.toString() + "\n";
			temp = temp.Next;
		}

		return retVal;
	}
}
