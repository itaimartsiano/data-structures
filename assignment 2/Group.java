public class Group {
	public String GroupName;
	private UserListNode Head;
	public Group Next;

	private UserListNode Tail;

	public Group() {

	}

	public Group(String GroupName) {
		this();
		this.GroupName = GroupName;
	}

	public Group(String GroupName, User user) {
		this(GroupName);
		UserListNode newUserNode = new UserListNode();
		newUserNode.CurrUser = user;
		this.Head = newUserNode;
		this.Tail = newUserNode;
	}

	public void addMember(User member) {

		UserListNode newUser = new UserListNode();
		newUser.CurrUser = member;
		Tail.Next = newUser;
		Tail = Tail.Next;

	}

	public void sendMessage(Message msg) {
		UserListNode curr = Head;

		while (curr != null) {
			curr.CurrUser.receiveMessage(msg, this);
			curr = curr.Next;
		}

	}

	public void removeUser(String userName) {

		// look for the user node and save prev and curr
		UserListNode curr = Head, prev = Head;
		boolean found = false;

		while (curr != null && !found) {
			found = curr.CurrUser.Name.equals(userName);
			if (!found) {
				prev = curr;
				curr = curr.Next;
			}
		}

		// remove conversion from user conversations list (curr)

		curr.CurrUser.removeConversation(GroupName);

		// remove user from user list node

		if (curr == prev) {
			Head = curr.Next;
		} else {
			prev.Next = curr.Next;
		}

	}

	public boolean isEmpty() {
		return Head == null;
	}

	public String toString() {
		String retVal = GroupName + "\n";
		UserListNode temp = Head;
		while (temp != null) {
			retVal += temp.CurrUser.Name + "\n";
			temp = temp.Next;
		}
		return retVal;
	}
}