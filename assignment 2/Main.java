
public class Main {
	public static void main(String[] args) 
	{
		Whatsapp wa = new Whatsapp();
		wa.AddUser("John");
		wa.AddUser("Paul");
		wa.AddUser("George");
		wa.AddUser("Ringo");
		wa.AddUser("Yoko");
		wa.AddUser("Martin");
		//check wa.userList
		//the new user is last
		
		
		
		wa.CreateGroup("John","Beatles");
		//check wa.userList
		//check group user list
		//check user conversation queue
		
		wa.AddMemberToGroup("John","George","Hey George","Beatles");
		//check the flowing:
		//conversation list of every user in the group - the conversation is first
		//user list of this list - the new member is last
		//message list of the relevant conversation for each user in the group
		
		wa.AddMemberToGroup("George","Ringo","Hey Ringo","Beatles");
		wa.SendMessage("John","I like Yoko","Beatles");
		//conversation list of every user in the group - the conversation is first
		//check if the message is linked - new massage is Tail
		
		
		
		wa.CreateGroup("John","John&Yoko");
		System.out.println("***This the current users list");
		System.out.println(wa.GetUsersList());
		System.out.println("***This the current groups list");
		System.out.println(wa.GetGroupsList());
		System.out.println("***These are the current details of the group - John&Yoko");
		System.out.println(wa.GetGroupDetails("John&Yoko"));
		wa.AddMemberToGroup("John","Yoko","Love me do","John&Yoko");
		System.out.println("***This is the current converstaion at John's phone in the group - John&Yoko");
		System.out.println(wa.GetConversationByUserAndGroup("John","John&Yoko"));
		System.out.println("***This is the current converstaion at John's phone in the group - Beatles");
		System.out.println(wa.GetConversationByUserAndGroup("John","Beatles"));
		System.out.println("***This is the current converstaion list at John's phone");
		System.out.println(wa.GetUserConversationList("John"));
		System.out.println("***These are all the messages containing the text 'He' in John's phone in the group - Beatles");
		
		
		System.out.println(wa.GetSearchMessageByTextString("John", "He","Beatles"));
		//checking that message order is from old to new
		//check case message not found -> return null
		System.out.println("<< OUR CHECK >>");
		System.out.println("send message John to Beatles with text  aaa");
		wa.SendMessage("John", "aaa1 olld", "Beatles");
		wa.SendMessage("John", "aaa2", "Beatles");
		wa.SendMessage("John", "aaa3", "Beatles");
		wa.SendMessage("John", "aaa4", "Beatles");
		wa.SendMessage("John", "aaa5", "Beatles");
		wa.SendMessage("John", "aaa6", "Beatles");
		wa.SendMessage("John", "aaa7 newww", "Beatles");
		System.out.println(wa.GetSearchMessageByTextString("John", "aaa","Beatles"));
		
		wa.RemoveUserFromGroup("John","Beatles");
		wa.RemoveUserFromGroup("George","Beatles");
		//check the group.userList
		//check user.conversationList
		//Radical case - the group should deleted - check wa.groupList
		
		System.out.println(wa.GetUserConversationList("John"));
		System.out.println(wa.GetUserConversationList("George"));
		
		System.out.println("***This the current groups list");
		System.out.println(wa.GetGroupsList());
		wa.RemoveUserFromGroup("Ringo","Beatles");
		System.out.println("***This the current groups list");
		System.out.println(wa.GetGroupsList());
		
		System.out.println("END");
		
		/*
		 * <--MUST DO BEFORE THURSDAY-->
		 * EAT, DRINK
		 * CHECK THE O(N) ASIMPTOTIC COMPLEX
		 * 
		 *   
		 * 
		 */
		
//		Whatsapp wa = new Whatsapp();
//		wa.AddUser("User1");
//		wa.AddUser("User2");
//		wa.AddUser("User3");
//		wa.AddUser("User4");
//		wa.AddUser("User5");
//		wa.AddUser("User6");
//		wa.AddUser("User7");
//		wa.AddUser("User8");
//		wa.AddUser("User9");
//		wa.AddUser("User10");
//		System.out.println("****Test1: User1 to User 10****");
//		System.out.println(wa.GetUsersList());
//		wa.CreateGroup("User1","Group1");
//		wa.CreateGroup("User2","Group2");
//		wa.CreateGroup("User3","Group3");
//		wa.CreateGroup("User5","Group5");
//		System.out.println("****Test2: need print (Group1, Group2, Group3, Group5)****");
//		System.out.println(wa.GetGroupsList());
//		wa.AddMemberToGroup("User1","User3","Hey User3: madafaka","Group1");
//		wa.AddMemberToGroup("User1","User5","Hey User5: madafaka","Group1");
//		wa.AddMemberToGroup("User1","User7","Hey User7: madafaka","Group1");
//		wa.SendMessage("User3", "User 5 do u want to join user 9 ? ", "Group1");
//		wa.SendMessage("User5", "Sure", "Group1");
//		wa.AddMemberToGroup("User5","User9","Hey User9: madafaka","Group1");
//		System.out.println("****Test3: Group1 Details:: User1, User3, User5, User7, User9****");
//		System.out.println(wa.GetGroupDetails("Group1"));
//		wa.AddMemberToGroup("User2","User4","Hey User4: madafaka","Group2");
//		wa.AddMemberToGroup("User4","User6","Hey User6: madafaka","Group2");
//		wa.AddMemberToGroup("User4","User8","Hey User8: madafaka","Group2");
//		wa.AddMemberToGroup("User3", "User1", "Welcome to 1to4 group", "Group3");
//		wa.AddMemberToGroup("User3", "User2", "hey user2 Welcome to 1to4 group", "Group3");
//		wa.AddMemberToGroup("User1", "User4", "hey user4 Welcome to 1to4 group", "Group3");
//		System.out.println("****Test4: User2 Details::Group3, Group2. cons of Group2 : hey User4, hey user6, hey User8**** ");
//		System.out.println(wa.GetUserConversationList("User2"));
//		System.out.println(wa.GetConversationByUserAndGroup("User2", "Group2"));
//		wa.SendMessage("User8", "thank you", "Group2");
//		System.out.println("****test5: need to see user8: thank you****");
//		System.out.println(wa.GetConversationByUserAndGroup("User2", "Group2"));
//		System.out.println("****test6: U'll see group2 users:: users2-8****");
//		System.out.println(wa.GetGroupDetails("Group2"));
//		System.out.println("****test7: user1 details: group3, group1 ****");
//		System.out.println("group 3 cons: tree messages");
//		System.out.println("group 1 cons: six message "
//				+ "");
//		System.out.println(wa.GetUserConversationList("User1"));
//		System.out.println(wa.GetConversationByUserAndGroup("User1", "Group3"));
//		System.out.println(wa.GetConversationByUserAndGroup("User1", "Group1"));
//		System.out.println("****Test 8: user3 details: Group3, Group1****");
//		System.out.println("group 3 cons: tree messages");
//		System.out.println("group 1 cons: six message ");
//		System.out.println(wa.GetUserConversationList("User3"));
//		System.out.println(wa.GetConversationByUserAndGroup("User3", "Group3"));
//		System.out.println(wa.GetConversationByUserAndGroup("User3", "Group1"));
//		wa.AddMemberToGroup("User5", "User3", "Hello User3", "Group5");
//		System.out.println("****Test 9: User3 details: Group5, Group3, Group1****");
//		System.out.println(wa.GetUserConversationList("User3"));
//		wa.SendMessage("User3", "hello user4", "Group3");
//		System.out.println("****Test 10: User3 details: Group3, Group5, Group1****");
//		System.out.println("group 3 cons: four messages");
//		System.out.println(wa.GetUserConversationList("User3"));
//		System.out.println(wa.GetConversationByUserAndGroup("User3", "Group3"));
//		System.out.println("****Test 11 : searching messages (need to find 3 messages)****");
//		wa.SendMessage("User1", "I like Psychedelic Trance", "Group1");
//		wa.SendMessage("User3", "Yeah! I like Goa", "Group1");
//		wa.SendMessage("User1", "Yes Goa is good", "Group1");
//		wa.SendMessage("User3", "but why do u like it?", "Group1");
//		wa.SendMessage("User1", "because Psychedelic Trance gives you a lot of energy", "Group1");
//		wa.SendMessage("User3", "yes, it true, who do u like most", "Group1");
//		wa.SendMessage("User1", "hard question for me I like Most X-Dream", "Group1");
//		wa.SendMessage("User3", "I like SKAZI", "Group1");
//		wa.SendMessage("User1", "ohh bad SKAZI is not Psychedelic ", "Group1");
//		System.out.println(wa.GetSearchMessageByTextString("User1", "Psychedelic","Group1"));
//		wa.RemoveUserFromGroup("User5", "Group5");
//		System.out.println("****test12: remove friends from group5 (need to print User3 and see only group 1 at user5**** ");
//		System.out.println(wa.GetGroupDetails("Group5"));
//		System.out.println(wa.GetUserConversationList("User5"));
//		System.out.println("****test 13: remove friends and group (need not see group5)****");
//		wa.RemoveUserFromGroup("User3", "Group5");
//		System.out.println(wa.GetGroupsList());
//		System.out.println("****test14: remove friend 6 (befor and after)****");
//		System.out.println(wa.GetGroupDetails("Group2"));
//		wa.RemoveUserFromGroup("User6", "Group2");
//		System.out.println(wa.GetGroupDetails("Group2"));
//		
//		System.out.println("End of HAVER SHEL OR");
		
//		Whatsapp wa = new Whatsapp();
//		wa.AddUser("John");
//		wa.AddUser("Paul");
//		wa.AddUser("George");
//		wa.AddUser("Ringo");
//		wa.AddUser("Yoko");
//		wa.AddUser("Martin");
//		wa.CreateGroup("John","Beatles");
//		wa.AddMemberToGroup("John","George","Hey George","Beatles");
//		wa.AddMemberToGroup("George","Ringo","Hey Ringo","Beatles");
//		wa.SendMessage("John","I like Yoko","Beatles");
//		wa.CreateGroup("John","John&Yoko");
//		System.out.println("***This the current users list");
//		System.out.println(wa.GetUsersList());
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		System.out.println("***These are the current details of the group - John&Yoko");
//		System.out.println(wa.GetGroupDetails("John&Yoko"));
//		wa.AddMemberToGroup("John","Yoko","Love me do","John&Yoko");
//		System.out.println("***This is the current converstaion at John's phone in the group - John&Yoko");
//		System.out.println(wa.GetConversationByUserAndGroup("John","John&Yoko"));
//		System.out.println("***This is the current converstaion at John's phone in the group - Beatles");
//		System.out.println(wa.GetConversationByUserAndGroup("John","Beatles"));
//		System.out.println("***This is the current converstaion list at John's phone");
//		System.out.println(wa.GetUserConversationList("John"));
//		System.out.println("***These are all the messages containing the text 'He' in John's phone in the group - Beatles");
//		System.out.println(wa.GetSearchMessageByTextString("John", "He","Beatles"));
//		wa.RemoveUserFromGroup("John","Beatles");
//		wa.RemoveUserFromGroup("George","Beatles");
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		wa.RemoveUserFromGroup("Ringo","Beatles");
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		
//		wa.AddUser("Noam");
//		wa.AddUser("Yosi");
//		wa.AddUser("Shlomo");
//		wa.AddUser("Boris");
//		System.out.println("***the users Noam, Yosi, Shlomo and Boris were added" );
//		System.out.println("***This the current user list");
//		System.out.println(wa.GetUsersList());
//		wa.CreateGroup("Noam","Fuck This");
//		wa.CreateGroup("Noam","Fuck that");
//		wa.CreateGroup("Noam","Fuck you");
//		wa.CreateGroup("Noam","Fuck me");
//		wa.CreateGroup("Noam","Fuck This Shit");
//		//addition
//		
//		System.out.println("WA group list");
//		System.out.println(wa.GetGroupsList());
//		
//		
//		//end of addition
//		System.out.println("***the groups , 'Fuck This','Fuck that', 'Fuck you', 'Fuck me' and  'Fuck This Shit',  was added with Noam as the founder and only user" );
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		wa.RemoveUserFromGroup("Noam","Fuck This");
//		wa.RemoveUserFromGroup("Noam","Fuck that");
//		wa.RemoveUserFromGroup("Noam","Fuck you");
//		System.out.println("***Noam was removed from the gruops the 'Fuck This','Fuck that', and 'Fuck you'  (therefore the groups should be deleted" );
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		wa.AddMemberToGroup("Noam","Yosi","Fuck you Yosi","Fuck This Shit");
//		System.out.println("***the user Yosi  was added to the group -'Fuck This Shit'" );
//		System.out.println("***This is the current converstaion at Yosi's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetConversationByUserAndGroup("Yosi","Fuck This Shit"));
//		wa.AddMemberToGroup("Noam","Shlomo","Fuck you Shlomo","Fuck This Shit");
//		System.out.println("***This is the current converstaion at Shlomo's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetConversationByUserAndGroup("Shlomo","Fuck This Shit"));
//		wa.AddMemberToGroup("Noam","Boris","Fuck you Boris","Fuck This Shit");
//		wa.AddMemberToGroup("Noam","Martin","Fuck you Martin","Fuck This Shit");
//		System.out.println("***This is the current converstaion at Yosi's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetConversationByUserAndGroup("Yosi","Fuck This Shit"));
//		wa.AddMemberToGroup("Noam","Yoko","Fuck you Yoko","Fuck This Shit");
//		wa.AddMemberToGroup("Noam","Ringo","Fuck you Ringo","Fuck This Shit");
//		wa.AddMemberToGroup("Noam","George","Fuck you George","Fuck This Shit");
//		wa.AddMemberToGroup("Noam","Paul","Fuck you Paul","Fuck This Shit");
//		System.out.println("***This is the current converstaion at Noam's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetConversationByUserAndGroup("Noam","Fuck This Shit"));
//		wa.SendMessage("Noam", "Fuck you all !!!", "Fuck This Shit");
//		System.out.println("***This is the current converstaion at Yosi's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetConversationByUserAndGroup("Yosi","Fuck This Shit"));
//		System.out.println("***These are all the messages containing the text 'Fuck' in Noam's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetSearchMessageByTextString("Noam", "Fuck","Fuck This Shit"));
//		System.out.println("***These are all the messages containing the text 'Boris' in Noam's phone in the group - Fuck This Shit");
//		System.out.println(wa.GetSearchMessageByTextString("Noam", "Boris","Fuck This Shit"));
//		wa.AddMemberToGroup("John","Noam"," Hi Noam","John&Yoko");
//		wa.SendMessage("Noam", "Fuck you John", "John&Yoko");
//		System.out.println("***These are all the messages containing the text 'Fuck' in Noam's phone in the group - John&Yoko");
//		System.out.println(wa.GetSearchMessageByTextString("Noam", "Fuck","John&Yoko"));
//		System.out.println("***These are all the messages containing the text 'Love' in John's phone in the group - John&Yoko");
//		System.out.println(wa.GetSearchMessageByTextString("John", "Love","John&Yoko"));
//		wa.RemoveUserFromGroup("John","John&Yoko");
//		wa.RemoveUserFromGroup("Yoko","John&Yoko");
//		System.out.println("***These are the current details of the group - John&Yoko");
//		System.out.println(wa.GetGroupDetails("John&Yoko"));
//		wa.RemoveUserFromGroup("Noam","John&Yoko");
//		System.out.println("***This the current groups list");
//		System.out.println(wa.GetGroupsList());
//		
//		System.out.println("MADAFAKA 2 END");
	
	}
	
}
