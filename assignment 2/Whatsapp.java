import java.awt.HeadlessException;

public class Whatsapp {
	UserListNode UsersHead;
	Group GroupsHead;

	UserListNode UsersTail;
	Group GroupsTail;

	public void AddUser(String inUserName) {
		UserListNode newUserNode = new UserListNode();
		newUserNode.CurrUser = new User(inUserName);

		if (UsersHead == null) {
			UsersHead = newUserNode;
			UsersTail = newUserNode;
		} else {
			UsersTail.Next = newUserNode;
			UsersTail = UsersTail.Next;
		}
	}

	public void CreateGroup(String inInitiator, String inGroupName) {
		UserListNode curr = UsersHead;
		boolean found = false;
		while (curr != null && !found) {
			found = curr.CurrUser.Name.equals(inInitiator);
			if (!found) {
				curr = curr.Next;
			}
		}

		Group newGroup = new Group(inGroupName, curr.CurrUser);

		if (GroupsHead == null) {
			GroupsHead = newGroup;
			GroupsTail = newGroup;
		} else {
			GroupsTail.Next = newGroup;
			GroupsTail = GroupsTail.Next;
		}
	}

	// finds the group from the list by group name
	private Group findGroup(String groupName) {
		Group curr = GroupsHead;
		boolean found = false;

		while (curr != null && !found) {
			found = curr.GroupName.equals(groupName);
			if (!found) {
				curr = curr.Next;
			}
		}
		return curr;
	}

	// finds the user from the list by user name
	private User findUser(String userName) {
		UserListNode curr = UsersHead;
		boolean found = false;

		while (curr != null && !found) {
			found = curr.CurrUser.Name.equals(userName);
			if (!found) {
				curr = curr.Next;
			}
		}

		return curr == null ? null : curr.CurrUser;
	}

	public void AddMemberToGroup(String invitorUser, String invitedUser,
			String inWelcmeText, String inGroupName) {
		// look for the group
		Group group = findGroup(inGroupName);

		// look for the added user
		User member = findUser(invitedUser);

		// Relying on the fact that member!=null and group!=null

		// add member
		group.addMember(member);
		// send message
		Message msg = new Message(invitorUser, inWelcmeText);
		group.sendMessage(msg);

	}

	public void SendMessage(String inSender, String inText, String inGroupName) {
		// look for the group
		Group group = findGroup(inGroupName);

		// create new message
		Message msg = new Message(inSender, inText);

		group.sendMessage(msg);

	}

	public void RemoveUserFromGroup(String inUserName, String inGroupName) {
		// look for the group - copy the code, save prev and curr for deletion
		Group curr = GroupsHead, prev = GroupsHead;
		boolean found = false;
		while (curr != null && !found) {
			found = curr.GroupName.equals(inGroupName);
			if (!found) {
				prev = curr;
				curr = curr.Next;
			}
		}

		// delete the user from the group (by name (string))
		// Relay on the fact curr!=null
		curr.removeUser(inUserName);

		// if group is empty - delete
		// if curr==prev -> head=curr.next else prev.next=curr.next
		if (curr.isEmpty()) {
			if (curr == prev) {
				GroupsHead = curr.Next;
			} else {
				prev.Next = curr.Next;
			}
		}
	}

	public Message[] SearchMessageByText(String inUserName, String inText,
			String inGroupName) {
		// look for the user
		User user = findUser(inUserName);

		// new array = user.searchmessage by text
		return user.searchMessageByText(inGroupName, inText);
	}

	public String GetSearchMessageByTextString(String inUserName,
			String inText, String inGroupName) {
		Message[] msgs = SearchMessageByText(inUserName, inText, inGroupName);
		String retVal = "";
		for (int i = 0; i < msgs.length; i++) {
			retVal += msgs[i].toString() + "\n";
		}

		return retVal;
	}

	public String GetUsersList() {
		String retVal = "";
		UserListNode temp = UsersHead;
		while (temp != null) {
			retVal += temp.CurrUser.Name + "\n";
			temp = temp.Next;
		}

		return retVal;
	}

	public String GetGroupsList() {
		String retVal = "";
		Group temp = GroupsHead;
		while (temp != null) {
			retVal += temp.GroupName + "\n";
			temp = temp.Next;
		}
		return retVal;
	}

	public String GetConversationByUserAndGroup(String inUserName,
			String inGroupName) {
		UserListNode temp = UsersHead;
		while (temp != null) {
			if (temp.CurrUser.Name.equals(inUserName)) {
				return temp.CurrUser.GetConversationByGroupName(inGroupName);
			}
			temp = temp.Next;
		}

		return null;
	}

	public String GetGroupDetails(String inGroupName) {
		Group temp = GroupsHead;
		while (temp != null) {
			if (temp.GroupName.equals(inGroupName)) {
				return temp.toString();
			}

			temp = temp.Next;
		}

		return null;
	}

	public String GetUserConversationList(String inUserName) {
		UserListNode temp = UsersHead;
		while (temp != null) {
			if (temp.CurrUser.Name.equals(inUserName)) {
				return temp.CurrUser.GetConversationList();

			}
			temp = temp.Next;
		}

		return null;
	}

}
