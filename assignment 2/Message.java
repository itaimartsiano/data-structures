
public class Message {
	public String SenderName;
	public String Text;
	public Message Next;
	
	public Message(){
	}
	
	public Message(String SenderName,String Text){
		this.SenderName=SenderName;
		this.Text=Text;
	}

	public Message(String SenderName,String Text, Message Next){
		this(SenderName,Text);
		this.Next=Next;
	}

	public String toString()
	{
		return SenderName+":"+Text;
	}
}
