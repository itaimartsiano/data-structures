import java.awt.HeadlessException;

public class User {
	public String Name;
	private Conversation ConversationsQueue;

	public User() {
		Name = null;
		ConversationsQueue = null;
	}

	public User(String name) {
		this();
		this.Name = name;
	}

	// Receive
	public void receiveMessage(Message msg, Group group) {

		Conversation curr = ConversationsQueue, prev = ConversationsQueue;
		boolean found = false;

		while (curr != null && !found) {
			found = curr.ConversationGroupName.equals(group.GroupName);
			if (!found) {
				prev = curr;
				curr = curr.Next;
			}
		}

		// the conversation is not found or queue is empty
		if (curr == null) {
			Conversation newConversation = new Conversation(group.GroupName);

			newConversation.Next = ConversationsQueue;
			ConversationsQueue = newConversation;
		}
		// the conversation is found and in the middle of list
		else if (ConversationsQueue != curr) {
			prev.Next = curr.Next;
			curr.Next = ConversationsQueue;
			ConversationsQueue = curr;

		}
		ConversationsQueue.insertMessage(msg);
	}

	public Message[] searchMessageByText(String groupName, String text) {
		// look for the conversion
		Conversation curr = ConversationsQueue;
		boolean found = false;
		while (curr != null & !found) {
			found = curr.ConversationGroupName.equals(groupName);
			if (!found) {
				curr = curr.Next;
			}
		}

		return curr==null ? null : curr.searchMessage(text); // conversation.searchmessage by text
	}

	public void removeConversation(String groupName) {

		// look for the conversation by group name - save the curr and prev
		Conversation curr = ConversationsQueue, prev = ConversationsQueue;
		boolean found = false;

		while (curr != null && !found) {
			found = curr.ConversationGroupName.equals(groupName);
			if (!found) {
				prev = curr;
				curr = curr.Next;
			}
		}

		if(curr==null){
			return;
		}
		
		// remove
		if (curr == prev) {
			ConversationsQueue = curr.Next;
		} else {
			prev.Next = curr.Next;
		}
	}

	public String GetConversationByGroupName(String inGroupName) {
		Conversation temp = ConversationsQueue;
		while (temp != null) {
			if (temp.ConversationGroupName.equals(inGroupName)) {
				return temp.toString();
			}
			temp = temp.Next;
		}

		return null;
	}

	public String GetConversationList() {
		String retVal = "";
		Conversation temp = ConversationsQueue;
		while (temp != null) {
			retVal += temp.ConversationGroupName + "\n";
			temp = temp.Next;
		}
		return retVal;
	}
}
